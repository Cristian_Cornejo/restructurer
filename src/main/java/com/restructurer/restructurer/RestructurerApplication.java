package com.restructurer.restructurer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestructurerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestructurerApplication.class, args);
	}

}
